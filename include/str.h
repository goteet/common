#ifndef GSO_UTILITY_STRING_H
#define GSO_UTILITY_STRING_H
namespace GSO
{
    class str_impl;
	class cstr;
	class dstr;
	class str;

	///normal string without manager
	///the string buffer is managed by the objectself.
	class str
	{
	public:
		str();
		str(const char*);
		str(const str&);
		~str();

		const char* c_str() const;
		const str& operator=(const str& rhs);
		

		int length() const;
		//str substr() const ;
		//int find();
		//int find_inv();

	private:
		str_impl* m_pString;
	};

	///const string
	///the value included can not be modified.
	///the const strings are managed by cstrmgr;
	class cstr
	{
	public:
		cstr();
		~cstr();
		cstr(const char*);
		cstr(const cstr&);
		
		const char* c_str() const;
		const cstr& operator=(const cstr& rhs);

		int length() const;
		//dstr substr() const ;
		//int find();
		//int find_inv();

	private:
		str_impl* m_pString;
	};

	///dynamic string
	///the value can be modified.
	///and it replace the std::string in stl.
	///since the buffer is managed by dstrmgr.
    class dstr
    {
    public:
        dstr();
		~dstr();
        dstr(const char*);
        dstr(const dstr&);
        dstr(const cstr&);

        const char* c_str() const;
		const dstr& operator=(const cstr&);
		const dstr& operator=(const dstr&);

		int length() const;
        ///dstr substr() const ;
        //int find();
        //int find_inv();

    private:
        str_impl* m_pString;
    };
}
#endif
