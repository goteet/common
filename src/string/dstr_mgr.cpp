#include "str_impl.h"
#include <string.h>


////the pointer should be dealed with another way
///but I simply copy the cstr_manager to used.
GSO::dstr_manager* GSO::dstr_manager::instance()
{
	static dstr_manager inst;
	return &inst;
}

GSO::dstr_manager::dstr_manager()
	: m_nCount(1)
{
	m_vedstrings.reserve(DEF_VEC_SIZE);
	m_vedstrings.resize(m_vedstrings.capacity());
	
	///create default string
	
	str_impl* pStr = get_str("");
	m_vedstrings[0] = pStr;
}


GSO::dstr_manager::~dstr_manager()
{
	release_all();
}

GSO::str_impl* GSO::dstr_manager::get_def()
{
	str_impl* pString = m_vedstrings[0];
	pString->increase_ref();
	return pString;
}


GSO::str_impl* GSO::dstr_manager::get_str(const char* szString)
{
	if(szString == 0)
		return get_def();

	str_impl* pStr = find_str(szString);
	if(pStr == 0)
	{
		pStr = create_str(szString);
		pStr->increase_ref();
	}
	return pStr;
}

//in the entry function, the sz_string is tested with zero.
GSO::str_impl* GSO::dstr_manager::create_str( const char* szString)
{
	int nLength = strlen(szString);
	char* pBuff = create_buffer(nLength+1);
	memcpy(pBuff, szString, nLength);
	str_impl* pString = new str_impl(pBuff, nLength);

	add_to_manager(pString);

	return pString;
}

//in the entry function, the sz_string is tested with zero.
GSO::str_impl* GSO::dstr_manager::find_str(const char* szString)
{
	str_impl* pString = 0;
	for(int i = 0; i != m_nCount; i++)
	{
		pString = m_vedstrings[i];
		if(strcmp(szString, pString->c_str()) == 0)
		{
			return pString;
		}
	}
	return 0;
}

void GSO::dstr_manager::add_to_manager(str_impl* pString)
{
	
	if(m_nCount == m_vedstrings.capacity())
	{
		m_vedstrings.reserve(m_vedstrings.capacity()+DEF_VEC_SIZE);
		m_vedstrings.resize(m_vedstrings.capacity());
	}

	m_vedstrings[m_nCount] = pString;
	m_nCount++;
}

void GSO::dstr_manager::release_all()
{
	for(int i = 0; i != m_nCount; i++)
	{
		m_vedstrings[i]->decrease_ref();
		if(m_vedstrings[i]->ref_count() != 0)
		{
			///forget to decrease outside.
			///output logs.
			m_vedstrings[i]->release();
		}
		delete m_vedstrings[i];
	}
	m_vedstrings.clear();
	m_vedstrings.reserve(DEF_VEC_SIZE);
}

char* GSO::dstr_manager::create_buffer( int count )
{
	return new char[count];
}