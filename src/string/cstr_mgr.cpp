#include "str_impl.h"
#include <string.h>

GSO::cstr_manager* GSO::cstr_manager::instance()
{
	static cstr_manager inst;
	return &inst;
}

GSO::cstr_manager::cstr_manager()
	: m_nCount(1)
{
	m_vecStrings.reserve(DEF_VEC_SIZE);
	m_vecStrings.resize(m_vecStrings.capacity());
	
	///create default string
	
	str_impl* pStr = get_str("");
	m_vecStrings[0] = pStr;
}


GSO::cstr_manager::~cstr_manager()
{
	release_all();
}

GSO::str_impl* GSO::cstr_manager::get_def()
{
	str_impl* pString = m_vecStrings[0];
	pString->increase_ref();
	return pString;
}


GSO::str_impl* GSO::cstr_manager::get_str(const char* szString)
{
	if(szString == 0)
		return get_def();

	str_impl* pStr = find_str(szString);
	if(pStr == 0)
	{
		pStr = create_str(szString);
		pStr->increase_ref();
	}
	return pStr;
}

//in the entry function, the sz_string is tested with zero.
GSO::str_impl* GSO::cstr_manager::create_str( const char* szString)
{
	int nLength = strlen(szString);
	char* pBuff = create_buffer(nLength+1);
	memcpy(pBuff, szString, nLength);
	str_impl* pString = new str_impl(pBuff, nLength);

	add_to_manager(pString);

	return pString;
}

//in the entry function, the sz_string is tested with zero.
GSO::str_impl* GSO::cstr_manager::find_str(const char* szString)
{
	str_impl* pString = 0;
	for(int i = 0; i != m_nCount; i++)
	{
		pString = m_vecStrings[i];
		if(strcmp(szString, pString->c_str()) == 0)
		{
			return pString;
		}
	}
	return 0;
}

void GSO::cstr_manager::add_to_manager(str_impl* pString)
{
	
	if(m_nCount == m_vecStrings.capacity())
	{
		m_vecStrings.reserve(m_vecStrings.capacity()+DEF_VEC_SIZE);
		m_vecStrings.resize(m_vecStrings.capacity());
	}

	m_vecStrings[m_nCount] = pString;
	m_nCount++;
}

void GSO::cstr_manager::release_all()
{
	for(int i = 0; i != m_nCount; i++)
	{
		m_vecStrings[i]->decrease_ref();
		if(m_vecStrings[i]->ref_count() != 0)
		{
			///forget to decrease outside.
			///output logs.
			m_vecStrings[i]->release();
		}
		delete m_vecStrings[i];
	}
	m_vecStrings.clear();
	m_vecStrings.reserve(DEF_VEC_SIZE);
}

char* GSO::cstr_manager::create_buffer( int count )
{
	return new char[count];
}