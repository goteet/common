#include "str.h"
#include "str_impl.h"
GSO::dstr::dstr()
{
	m_pString = dstr_manager::instance()->get_def();
}
GSO::dstr::dstr(const char* szString)
{
	m_pString = dstr_manager::instance()->get_str(szString);
}

GSO::dstr::dstr(const GSO::cstr& cpy)
{
	m_pString = dstr_manager::instance()->get_str(cpy.c_str());
}

GSO::dstr::dstr(const GSO::dstr& cpy)
{
	m_pString = cpy.m_pString;
	m_pString->increase_ref();
}

const GSO::dstr& GSO::dstr::operator=( const dstr& rhs )
{
	if(this == &rhs)
		return rhs;
	if(m_pString != rhs.m_pString)
	{
		m_pString->decrease_ref();
		m_pString = rhs.m_pString;
		m_pString->increase_ref();
	}
	return *this;	
}

const GSO::dstr& GSO::dstr::operator=( const cstr& rhs )
{
	m_pString->decrease_ref();
	m_pString = dstr_manager::instance()->get_str(rhs.c_str());
	return *this;	
}

int GSO::dstr::length() const
{
	return m_pString->length();
}

const char* GSO::dstr::c_str() const
{
	return m_pString->c_str();
}


GSO::dstr::~dstr()
{
	m_pString->decrease_ref();
}
