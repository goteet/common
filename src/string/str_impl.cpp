#include "str_impl.h"
#include <string.h>

///the buffer is created by manager
GSO::str_impl::str_impl( const char* data, int len )
	: m_pString(data)
	, m_nLength(len)
	, m_nRefCount(1)
{

}

const char* GSO::str_impl::c_str() const
{
	return m_pString ? m_pString : "";
}

int GSO::str_impl::length() const
{
	return m_nLength;
}

GSO::str_impl::~str_impl()
{
	decrease_ref();
}

void GSO::str_impl::increase_ref()
{
	m_nRefCount++;
}

void GSO::str_impl::decrease_ref()
{
	m_nRefCount--;
	if(m_nRefCount == 0)
		release();
}

void GSO::str_impl::release()
{
	if(m_pString)
	{
		delete []m_pString;
		m_pString = 0;
		m_nLength = 0;
	}
}

int GSO::str_impl::ref_count() const
{
	return m_nRefCount;
}
