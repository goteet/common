#ifndef GSO_STRING_IMPLEMENT_H
#define GSO_STRING_IMPLEMENT_H

#include <vector>
namespace GSO
{
	class str_impl
	{
	public:
		str_impl(const char* data, int len);
		~str_impl();
		const char*	c_str() const;
		void	increase_ref();
		void	decrease_ref();
		int		length() const;
		int		ref_count() const;
		void	release();
	private:
		int			m_nRefCount;
		int			m_nLength;
		const char*	m_pString;
	};

	class cstr_manager
	{
	public:
		cstr_manager();
		~cstr_manager();
	public:
		static cstr_manager* instance();
		str_impl* get_def();
		str_impl* get_str(const char* );
	private:
		char*		create_buffer(int count);
		str_impl*	find_str(const char*);
		str_impl*	create_str(const char*);
		void		add_to_manager(str_impl*);
		void		release_all();
	private://container.
		/// this should be replace by the hash_vector/hash_table
		/// before these codes go into the project.
		static const int DEF_VEC_SIZE = 128;
		std::vector<str_impl*> m_vecStrings;
		int m_nCount;
	};

	class dstr_manager
	{
	public:
		dstr_manager();
		~dstr_manager();
	public:
		static dstr_manager* instance();
		str_impl* get_def();
		str_impl* get_str(const char* );
	private:
		char*		create_buffer(int count);
		str_impl*	find_str(const char*);
		str_impl*	create_str(const char*);
		void		add_to_manager(str_impl*);
		void		release_all();
	private://container.
		/// this should be replace by the hash_vector/hash_table
		/// before these codes go into the project.
		static const int DEF_VEC_SIZE = 128;
		std::vector<str_impl*> m_vedstrings;
		int m_nCount;
	};
}
#endif // GSO_STRING_IMPLEMENT_H