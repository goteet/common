#include "str.h"
#include "str_impl.h"

namespace{
	GSO::str_impl* create_str(const char* szString)
	{
		int nLength = 0;
		char* pBuffer;

		if(szString == 0)
		{
			pBuffer = new char[1];
		}
		else
		{
			nLength = strlen(szString);
			pBuffer = new char[nLength+1];
			memcpy(pBuffer, szString, nLength);
		}

		pBuffer[nLength] = '\0';

		return new GSO::str_impl(pBuffer,nLength);
	}
}
GSO::str::str()
{
	m_pString = create_str("");
}
GSO::str::str(const char* szString)
{
	m_pString = create_str(szString);
}

GSO::str::str(const GSO::str& cpy)
{
	m_pString = cpy.m_pString;
	m_pString->increase_ref();
}

const GSO::str& GSO::str::operator=( const str& rhs )
{
	if(this == &rhs)
		return rhs;
	if(m_pString != rhs.m_pString)
	{
		m_pString->decrease_ref();
		if(m_pString->ref_count() == 0)
		{
			delete m_pString;
		}
		m_pString = rhs.m_pString;
		m_pString->increase_ref();
	}
	return *this;	
}


const char* GSO::str::c_str() const
{
	return m_pString->c_str();
}


GSO::str::~str()
{
	m_pString->decrease_ref();
	if(m_pString->ref_count() == 0)
	{
		delete m_pString;
	}
}

int GSO::str::length() const
{
	return m_pString->length();
}