#include "str.h"
#include "str_impl.h"
GSO::cstr::cstr()
{
	m_pString = cstr_manager::instance()->get_def();
}
GSO::cstr::cstr(const char* szString)
{
	m_pString = cstr_manager::instance()->get_str(szString);
}
GSO::cstr::cstr(const GSO::cstr& cpy)
{
	m_pString = cpy.m_pString;
	m_pString->increase_ref();
}

const GSO::cstr& GSO::cstr::operator=( const cstr& rhs )
{
	if(this == &rhs)
		return rhs;
	if(m_pString != rhs.m_pString)
	{
		m_pString->decrease_ref();
		m_pString = rhs.m_pString;
		m_pString->increase_ref();
	}
	return *this;	
}

int GSO::cstr::length() const
{
	return m_pString->length();
}

const char* GSO::cstr::c_str() const
{
	return m_pString->c_str();
}


GSO::cstr::~cstr()
{
	m_pString->decrease_ref();
}
